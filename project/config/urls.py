from django.contrib import admin
from django.urls import path, include
from flights.views import index, detail


apipatterns = [
    path('flights/', include('flights.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(apipatterns)),
    path('', index),
    path('<str:route>/', detail, name='detail_url')
]
