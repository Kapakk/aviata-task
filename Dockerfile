# pull official base image
FROM python:3.8.3-alpine


# set work directory
RUN mkdir /src
WORKDIR /src


# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# Main packages
RUN apk add --no-cache libffi-dev postgresql-dev python3-dev musl-dev jpeg-dev zlib-dev libjpeg gcc


# install dependencies
ADD requirements.txt /src
RUN pip install --upgrade pip
RUN pip install -r requirements.txt


# copy project
ADD . /src

WORKDIR /src/project
