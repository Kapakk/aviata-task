from config.settings.extra import SKYPICKER_F_URL, SKYPICKER_C_URL
from django.core.cache import cache
import datetime
import requests


def send_get(url, params=None):
    r = requests.get(url, params=params)
    return r


def get_available_flights(fly_from, fly_to, date_from=None, date_to=None):
    params = {
        'fly_from': fly_from,
        'fly_to': fly_to,
        'partner': 'picky',
        'date_from': date_from,
        'date_to': date_to
    }
    r = send_get(SKYPICKER_F_URL, params=params)
    flights = [item for item in r.json()['data'] if item['availability']['seats']]
    data = []

    for flight in flights:
        # Timestamp to datetime
        flight['dTime'] = datetime.datetime.fromtimestamp(flight['dTime']).strftime('%Y-%m-%d %H:%M:%S')
        flight['aTime'] = datetime.datetime.fromtimestamp(flight['aTime']).strftime('%Y-%m-%d %H:%M:%S')

        # Get only need data
        new_dict = {}
        for key, value in flight.items():
            if key in ['price', 'dTime', 'aTime', 'availability', 'booking_token', 'id']:
                new_dict[key] = value
        data.append(new_dict)

    return data


def get_low_prices(data):
    flights = []

    for flight in data:
        if flight['dTime'][:10] not in [i['dTime'][:10] for i in flights]:
            flights.append(flight)
        else:
            for item in [i for i in flights if i['dTime'] == flight['dTime']]:
                if flight['price'] < item['price']:
                    flights[flights.index(item)] = flight

    return flights


def update_route(route):
    route_api = route.split('-')
    now = datetime.datetime.now()
    after_month = now + datetime.timedelta(days=30)
    available_flights = get_available_flights(
        fly_from=route_api[0].upper(),
        fly_to=route_api[1].upper(),
        date_from=f'{now.day}/{now.month}/{now.year}',
        date_to=f'{after_month.day}/{after_month.month}/{after_month.year}',
    )
    low_prices = get_low_prices(available_flights)
    cache.set(route, low_prices, timeout=None)
    data = low_prices
    return data