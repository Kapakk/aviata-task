from celery import shared_task
from celery.decorators import task
from config.settings.extra import ROUTES
from .utils import update_route


@shared_task
def update_flights():
    for route in ROUTES:
        update_route(route)


@task(name='update_route')
def update_route_task(route):
    update_route(route)

