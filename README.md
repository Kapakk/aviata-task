# **DOCUMENTATION**
   
**Запуск проекта:**
1) sudo docker-compose up -d --build

Данные обновляются каждый день в 00:00.
Если будет первый запрос, он будет длиться 2-4 секунды, так как кеш пустой.
   
### **FLIGHTS API** 

1. **api/flights/{route}/** - Tickets list

```
     ROUTES:
    'ala-tse',
    'tse-ala',
    'ala-mow',
    'mow-ala',
    'ala-cit',
    'cit-ala',
    'tse-mow',
    'mow-tse',
    'tse-led',
    'led-tse'
```
### **Template**
3. **localhost:8000/** - Index







