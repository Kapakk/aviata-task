from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from django.core.cache import cache
from django.shortcuts import render, redirect
from django.contrib import messages
from config.settings.extra import ROUTES, SKYPICKER_C_URL
from .utils import send_get, update_route
from .tasks import update_route_task


class FlightViewSet(viewsets.ViewSet):

    def list(self, request, route, *args, **kwargs):
        if route in ROUTES:
            if cache.get(route):
                data = cache.get(route)
            else:
                data = update_route(route)
            status_code = status.HTTP_200_OK
        else:
            status_code = status.HTTP_400_BAD_REQUEST
            data = {'error': 'Invalid route'}
        return Response(status=status_code, data=data)


def index(request):
    if request.method == 'POST':
        return redirect('detail_url', route=request.POST.get('flights'))
    return render(request, 'flights/base.html', context={'routes': ROUTES})


def detail(request, route):
    if request.method == 'POST':
        booking_token = request.POST.get('booking_token')
        if booking_token:
            params = {
                'currency': 'RUB',
                'bnum': 1,
                'v': 2,
                'pnum': 1,
                'affily': 'picky_us',
                'booking_token': booking_token
            }
            data = send_get(SKYPICKER_C_URL, params=params).json()

            invalid = data['flights_invalid']
            price_change = data['price_change']

            if price_change:
                update_route_task.delay(route)
                messages.error(request, 'Wait a minute, the price is updated.')
            if invalid is True:
                messages.error(request, 'You can not buy this ticket')
            else:
                messages.success(request, 'You can buy this ticket')

    base_url = f'{request.scheme}://{request.get_host()}'
    url = base_url + '/api/flights/' + route + '/'
    data = send_get(url)
    return render(request, 'flights/index.html', context={'data': data.json() if data else None})
