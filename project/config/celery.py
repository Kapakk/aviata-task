import os
from celery.schedules import crontab
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.development')
app = Celery('config')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    # Executes every Monday morning at 7:30 a.m.
    'update_flights_00:00': {
        'task': 'flights.tasks.update_flights',
        'schedule': crontab(minute=0, hour=0),
        'args': None,
    },
}