from django.urls import path
from .views import FlightViewSet


urlpatterns = [
    path('<str:route>/', FlightViewSet.as_view({'get': 'list'})),
]
