### SKYPICKER URLS ###

SKYPICKER_F_URL = 'https://api.skypicker.com/flights'
SKYPICKER_C_URL = 'https://booking-api.skypicker.com/api/v0.1/check_flights'

### REDIS CACHE ###

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": 'redis://redis:6379/0',
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

### FLIGHTS ###
ROUTES = [
    'ala-tse',
    'tse-ala',
    'ala-mow',
    'mow-ala',
    'ala-cit',
    'cit-ala',
    'tse-mow',
    'mow-tse',
    'tse-led',
    'led-tse'
]

### Celery ###
CELERY_BROKER_URL = 'redis://redis:6379/0'
CELERY_RESULT_BACKEND = 'redis://redis:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
